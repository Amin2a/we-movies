# We Movies

* PHP 8.O.0
* Symfony 5.4


## Installation dans une VM

1. Cloner le projet : `git@gitlab.com:Amin2a/we-movies.git`

2. Lancer l'application :
   * `cd infra`
   * `docker-compose up -d`
   * `docker-compose run --rm --no-deps php sh -c  '(composer install --prefer-dist)'`
   * `docker-compose run --rm --no-deps node sh -c  '(npm install --prefer-dist)'`
   * `docker-compose run --rm --no-deps node sh -c  '(npm run build --prefer-dist)'`
   

3. Lancer via le navigateur : `http://localhost/index`


