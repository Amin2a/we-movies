<?php

namespace App\Service;


interface MovieInterface
{

    public function getGenre(): array;

    public function getTopRated(): array;

    public function getMoviesByGenre(int $idGenre): array;

    public function getMovieDetail(int $idMovie): array;

    public function getVideoMovieById(int $idMovie): array;

}