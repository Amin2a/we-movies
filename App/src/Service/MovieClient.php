<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class MovieClient
{
    private HttpClientInterface $httpClient;
    private string $piKey;

    public function __construct(HttpClientInterface $httpClient, string $apiKey)
    {
        $this->httpClient = $httpClient;
        $this->apiKey = $apiKey;
    }

    public function request(string $method, string $url, array $options = []): array
    {

        $options['query']['api_key'] = $this->apiKey;
        $response = $this->httpClient->request($method, $url, $options);
        return $response->toArray();

    }

}