<?php

namespace App\Service;

class MovieService implements MovieInterface
{
    const URL_ALL_GENRE = '/3/genre/movie/list';
    const URL_TOP_RATED = '/3/movie/top_rated';
    const URL_VIDEO_TOP_RATED = '/3/movie/[id_movie]/videos';
    const URL_MOVIES_BY_GENRE = '/3/discover/movie';
    const URL_MOVIE_DEATAIL = '/3/movie/[id_movie]';

    private MovieClient $movieClient;


    public function __construct(MovieClient $movieClient)
    {
        $this->movieClient = $movieClient;

    }

    public function getGenre(): array
    {
        return $this->movieClient->request('GET',  self::URL_ALL_GENRE);

    }
    public function getTopRated(): array
    {
        $response =  $this->movieClient->request('GET',  self::URL_TOP_RATED);
        return $response['results'][5];
    }

    public function getVideoMovieById(int $idMovie): array
    {
        $response =  $this->movieClient->request('GET',  str_replace('[id_movie]', $idMovie, self::URL_VIDEO_TOP_RATED));
        return $response['results'][0];
    }
    public function getMoviesByGenre(int $idGenre): array
    {
        $options['query']['with_genres'] = $idGenre;
        $response =  $this->movieClient->request('GET',  self::URL_MOVIES_BY_GENRE, $options);

        return $response['results'];
    }
    public function getMovieDetail(int $idMovie): array
    {
        $response =  $this->movieClient->request('GET',  str_replace('[id_movie]', $idMovie, self::URL_MOVIE_DEATAIL));

        return $response;
        // TODO: Implement getMovieDetail() method.
    }

}