<?php

namespace App\Controller;

use App\Service\MovieInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class MovieController extends AbstractController
{

    public function index(MovieInterface $movieService)
    {
       $movie = $movieService->getTopRated();
       $keyVideo = $movieService->getVideoMovieById($movie['id']);
        return $this->render('index.html.twig',
            [
                'topRated' => $movie,
                'keyVideo' => $keyVideo,
            ],
        );
    }

    public function getAllGendre(MovieInterface $movieService)
    {
        return $this->render('gendre.html.twig',
            [
                'genre' => $movieService->getGenre()
            ]);
    }


    public function search()
    {
        return $this->render('search.html.twig');
    }


    public function getMoviesByGenre(MovieInterface $movieService, int $idGenre): Response
    {
        return $this->render('movies_by_genre.html.twig',
            ['moviesByGenre' =>  $movieService->getMoviesByGenre($idGenre)]
        );

    }

    public function getDetailMovie(MovieInterface $movieService, int $idMovie)
    {
        $movie = $movieService->getMovieDetail($idMovie);
        $keyVideo = $movieService->getVideoMovieById($movie['id']);
        return $this->render('detail.html.twig',
            [
                'detailMovie' => $movie,
                'keyVideo' => $keyVideo,
            ],
        );
    }

}